from django.contrib import admin

from .models import Course, Teacher, Student, Account, Topic, Lesson, Schedule, Homework

admin.site.register(Course)
admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(Account)
admin.site.register(Topic)
admin.site.register(Lesson)
admin.site.register(Schedule)
admin.site.register(Homework)