from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from .forms import CourseForm
from .models import Course
from .serializers import CourseSerializer


class FormMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form
        return context


class CourseListView(LoginRequiredMixin, FormMixin, ListView):
    model = Course
    form = CourseForm
    fields = ['name']


class CourseCreateView(CreateView):
    model = Course
    fields = ['name']
    success_url = reverse_lazy('courses-list')


class CourseUpdateView(UpdateView):
    model = Course
    form = CourseForm
    fields = ['name']


class CourseDeleteView(DeleteView):
    model = Course
    success_url = reverse_lazy('courses-list')


class CourseViewSet(ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [IsAuthenticated]
