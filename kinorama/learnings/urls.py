from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import CourseListView, CourseCreateView, CourseUpdateView, CourseDeleteView, CourseViewSet

router = DefaultRouter()
router.register("courses", CourseViewSet)

urlpatterns = [
    path('courses/', CourseListView.as_view(), name='courses-list'),
    path('courses/create/', CourseCreateView.as_view(), name='course-create'),
    path('courses/<int:pk>/update', CourseUpdateView.as_view(), name='course-update'),
    path('courses/<int:pk>/delete', CourseDeleteView.as_view(), name='course-delete'),
    path('api-auth/', include('rest_framework.urls')),
    path("api/", include(router.urls)),
    path('accounts/', include('django.contrib.auth.urls')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]