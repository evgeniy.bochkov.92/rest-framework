from rest_framework import serializers

from learnings.models import Course


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = "id", "name"
        view_name = "courses"
