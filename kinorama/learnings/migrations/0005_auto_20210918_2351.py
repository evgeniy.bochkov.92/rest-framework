# Generated by Django 3.2.7 on 2021-09-18 18:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('learnings', '0004_auto_20210918_2348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата создания'),
        ),
        migrations.AlterField(
            model_name='course',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Дата обновления'),
        ),
    ]
